function change_batch_table(a){
  $("#hidethisyo").show();
  requestURL="https://timetraveller.pythonanywhere.com/leaderboard/"+a+"_overall";
  console.log(requestURL);
  $.getJSON(requestURL, function(data) {
      $("#overall_table").find("tr:gt(0)").remove();
      $("#branch_table").css("opacity", "0");
      $("#overall_table").css("opacity", "1");
      for(var i=1;i<11;i++){
        var row = overall_table.insertRow(i);
        var cell1=row.insertCell(0);
        var cell2=row.insertCell(1);
        var cell3=row.insertCell(2);
        if(a==17){
          cell1.innerHTML=data[i]['rank'];
        }
        else{
          cell1.innerHTML=i;
        }
        cell2.innerHTML=data[i]['name'];
        cell3.innerHTML=data[i]['cgpa'];
      };
    });
    $("#hidethisyo").hide();
}
function change_branch_table(a,b){
  $("body").css("overflow-y", "scroll");
  $("#hidethisyo2").show();
  requestURL="https://timetraveller.pythonanywhere.com/leaderboard/"+a+"_"+b;
  console.log(requestURL);
  $.getJSON(requestURL, function(data) {
      $("#branch_table").find("tr:gt(0)").remove();
      $("#branch_table").css("opacity", "1");
      for(var i=1;i<11;i++){
        var row = branch_table.insertRow(i);
        var cell1=row.insertCell(0);
        var cell2=row.insertCell(1);
        var cell3=row.insertCell(2);
        cell1.innerHTML=i;
        cell2.innerHTML=data[i]['name'];
        cell3.innerHTML=data[i]['cgpa'];
      };
    });
      $("#hidethisyo2").hide();
}

var year=-1;
$(document).ready(function(){
  $("body").css("overflow-y", "scroll");
  $("#hidethisyo").hide();
  $("#hidethisyo2").hide();
  var overall_table=document.getElementById('overall_table');
  var branch_table=document.getElementById('branch_table');
  $("#2k15").click(function(){
    $("#hidethisyo").show();
    change_batch_table(15);
    year=15;
  });
  $("#2k16").click(function(){
    change_batch_table(16);
    year=16;
  });
  $("#2k17").click(function(){
    change_batch_table(17);
    year=17;
  });
  $("#ae").click(function(){
    if(year==-1){
      alert("Select a batch first");
    }
    else if(year==17){
      alert("No branches as per departments have been made yet");
    }
    else{
      change_branch_table(year,"AE");
    }

  });
  $("#bt").click(function(){
    if(year==-1){
      alert("Select a batch first");
    }
    else if(year==17){
      alert("No branches as per departments have been made yet");
    }
    else{
      change_branch_table(year,"BT");
    }
  });
  $("#ce").click(function(){
    if(year==-1){
      alert("Select a batch first");
    }
    else if(year==17){
      alert("No branches as per departments have been made yet");
    }
    else{
      change_branch_table(year,"CE");
    }
  });
  $("#co").click(function(){
    if(year==-1){
      alert("Select a batch first");
    }
    else if(year==17){
      alert("No branches as per departments have been made yet");
    }
    else{
      change_branch_table(year,"CO");
    }
  });
  $("#ec").click(function(){
    if(year==-1){
      alert("Select a batch first");
    }
    else if(year==17){
      alert("No branches as per departments have been made yet");
    }
    else{
      change_branch_table(year,"EC");
    }
  });
  $("#ee").click(function(){
    if(year==-1){
      alert("Select a batch first");
    }
    else if(year==17){
      alert("No branches as per departments have been made yet");
    }
    else{
      change_branch_table(year,"EE");
    }
  });
  $("#el").click(function(){
    if(year==-1){
      alert("Select a batch first");
    }
    else if(year==17){
      alert("No branches as per departments have been made yet");
    }
    else{
      change_branch_table(year,"EL");
    }
  });
  $("#en").click(function(){
    if(year==-1){
      alert("Select a batch first");
    }
    else if(year==17){
      alert("No branches as per departments have been made yet");
    }
    else{
      change_branch_table(year,"EN");
    }
  });
  $("#ep").click(function(){
    if(year==-1){
      alert("Select a batch first");
    }
    else if(year==17){
      alert("No branches as per departments have been made yet");
    }
    else{
      change_branch_table(year,"EP");
    }
  });
  $("#it").click(function(){
    if(year==-1){
      alert("Select a batch first");
    }
    else if(year==17){
      alert("No branches as per departments have been made yet");
    }
    else{
      change_branch_table(year,"IT");
    }
  });
  $("#mc").click(function(){
    if(year==-1){
      alert("Select a batch first");
    }
    else if(year==17){
      alert("No branches as per departments have been made yet");
    }
    else{
      change_branch_table(year,"MC");
    }
  });
  $("#me").click(function(){
    if(year==-1){
      alert("Select a batch first");
    }
    else if(year==17){
      alert("No branches as per departments have been made yet");
    }
    else{
      change_branch_table(year,"ME");
    }
  });
  $("#pe").click(function(){
    if(year==-1){
      alert("Select a batch first");
    }
    else if(year==17){
      alert("No branches as per departments have been made yet");
    }
    else{
      change_branch_table(year,"PE");
    }
  });
  $("#ps").click(function(){
    if(year==-1){
      alert("Select a batch first");
    }
    else if(year==17){
      alert("No branches as per departments have been made yet");
    }
    else{
      change_branch_table(year,"PS");
    }
  });
  $("#se").click(function(){
    if(year==-1){
      alert("Select a batch first");
    }
    else if(year==17){
      alert("No branches as per departments have been made yet");
    }
    else{
      change_branch_table(year,"SE");
    }
  });

  $('#sidebarCollapse').click(function () {
      $('#sidebar').toggleClass('active');
  });
  $('#sidebarCollapse2').click(function () {
      $('#sidebar').toggleClass('active');
  });
});
