function load_chart_1(label_sem,data_rank){
  var ctx = document.getElementById("myChart").getContext('2d');
        console.log(label_sem);
  var myChart = new Chart(ctx, {
      type: 'line',
      data: {
          labels: label_sem,
          datasets: [{
              label: 'Rank',
              data: data_rank,
              borderColor: [
                  '#26ABDA',
              ],
              borderWidth: 1
          }]
      },
      options: {
          scales: {
              yAxes: [{
                  ticks: {
                      beginAtZero:true
                  }
              }]
          }
      }
  });
}
function load_chart_2(label_sem,data_dr_rank){
  var cty = document.getElementById("myChart2").getContext('2d');
  var myChart = new Chart(cty, {
      type: 'line',
      data: {
        labels: label_sem,
          datasets: [{
              label: 'Dr_Rank',
              data: data_dr_rank,

              borderColor: [
                  '#26ABDA',

              ],
              borderWidth: 1
          }]
      },
      options: {
          scales: {
              yAxes: [{
                  ticks: {
                      beginAtZero:true
                  }
              }]
          }
      }
  });

}
function load_chart_3(label_sem,data_gpa){
  var cty2 = document.getElementById("myChart3").getContext('2d');
  var myChart = new Chart(cty2, {
      type: 'doughnut',
      data: {
          labels: label_sem,
          datasets: [{
              label: '# of Votes',
              data: data_gpa,

              borderColor: [
                  '#fff',
                  '#26ABDA',
                  '#77CDEA',
                  '#4BBCE1',
                  '#079FD1',
                  '#037398'
              ],
              backgroundColor: [
                '#fff',
                '#26ABDA',
                '#77CDEA',
                '#4BBCE1',
                '#079FD1',
                '#037398'
              ],
              borderWidth: 1
          }]
      },

  });


}
function load_chart_4(label_sem,data_gpa){
  var cty0 = document.getElementById("myChart4").getContext('2d');

  var myChart = new Chart(cty0, {
      type: 'radar',
      data: {
        labels: label_sem,
          datasets: [{
              label: 'GPA Distribution',
              data: data_gpa,
              pointBackgroundColor: '#26ABDA',
              borderColor: ['#26ABDA'],
              borderWidth: 1
          }]
      },
      options: {
      }
  });

}

$(document).ready(function () {
  $("#hidethisyo").hide();
  document.getElementById("inp").value="Enter roll number (Ex. 2016/B2/345)";
  $("#inp").focus(function(){
    if(document.getElementById("inp").value==="Enter roll number (Ex. 2016/B2/345)"){
      document.getElementById("inp").value="";
    }
  });

  $(".my_btn").click(function(){
    $(".main_data").css("opacity", "0");
    stro = grecaptcha.getResponse()
    if(stro){
      $("#hidethisyo").show();
    }
    else{
      alert("Fill the captcha correctly!");
    }
     $("body").css("overflow-y", "scroll");
      var roll = document.getElementById("inp").value;
      roll=roll.toLowerCase();
      if(roll[1]=='k')
        roll = roll.replace("k","0");
      roll = roll.replace("/","-");
      roll = roll.replace("/","-");
      roll = roll.replace("/","-");

      if(stro){
          requestURL='https://timetraveller.pythonanywhere.com/?reg_id='+roll+"&g-recaptcha-response="+stro;
          console.log(requestURL);
      }
      else{
          //do nothing
      }

      $.getJSON(requestURL, function(data) {
        if(data['name']===undefined) {
            alert("Captcha not filled or data unavailable. If the problem persists visit 'reform query' page to submit us your data related problem so we can fix it.");
            console.log("Gomenasai! no data present for you desu >///< ");
        }
        else{
          console.log("we're here");
          grecaptcha.reset();
          $(".main_data").css("opacity", "1");
          var gpa=document.getElementById("gpa");
          var dr_rank=document.getElementById("dr_rank");
          var rank=document.getElementById("rank");
          var name=document.getElementById("name");
          var branch=document.getElementById("branch");
          var roll=document.getElementById("roll_no");
          var gpa_text=document.getElementById("gpa_text");
          var dr_text=document.getElementById("dr_text");
          var rank_text=document.getElementById("rank_text");
          gpa.innerHTML=data['cgpa'];
          dr_rank.innerHTML=data['dr_rank'];
          rank.innerHTML=data['rank'];
          branch.innerHTML=data['branch'];
          roll.innerHTML=data['roll'];
          name.innerHTML=data['name'];
          if(data['sgpa4']===undefined && data['sgpa5']===undefined && data['sgpa3']!=undefined && data['sgpa2']!=undefined){
            label_sem = ["Sem1", "Sem2", "Sem3"];
            data_dr_rank=[data["dr_rank_sem1"],data["dr_rank_sem2"],data["dr_rank_sem3"]];
            data_rank=[data["rank_sem1"],data["rank_sem2"],data["rank_sem3"]];
            data_gpa=[data["sgpa1"],data["sgpa2"],data["sgpa3"]];
            var gpa_text1="avg( "+data['sgpa1']+" | "+data['sgpa2']+" | "+data['sgpa3']+" )";
            var rank_text1="( "+data['rank_sem1']+" | "+data['rank_sem2']+" | "+data['rank_sem3']+" )";
            var dr_text1="( "+data['dr_rank_sem1']+" | "+data['dr_rank_sem2']+" | "+data['dr_rank_sem3']+" )";
            gpa_text.innerHTML=gpa_text1;
            dr_text.innerHTML=dr_text1;
            rank_text.innerHTML=rank_text1;
          }
          else if(data['sgpa2']===undefined && data['sgpa3']===undefined){
            label_sem = ["Sem1"];
            data_dr_rank=[data["rank"]];
            data_rank=[data["rank"]];
            data_gpa=[data["sgpa1"]];
            var gpa_text1="avg( "+data['sgpa1']+" )";
            var rank_text1="( "+data['rank']+" )";
            var dr_text1="( "+data['dr_rank_sem1']+" )";
            gpa_text.innerHTML=gpa_text1;
            dr_text.innerHTML=dr_text1;
            rank_text.innerHTML=rank_text1;
          }
          else{
            label_sem = ["Sem1", "Sem2", "Sem3", "Sem4", "Sem5"];
            data_dr_rank=[data["dr_rank_sem1"],data["dr_rank_sem2"],data["dr_rank_sem3"],data["dr_rank_sem4"],data["dr_rank_sem5"]];
            data_rank=[data["rank_sem1"],data["rank_sem2"],data["rank_sem3"],data["rank_sem4"],data["rank_sem5"]];
            data_gpa=[data["sgpa1"],data["sgpa2"],data["sgpa3"],data["sgpa4"],data["sgpa5"]];
            var gpa_text1="avg( "+data['sgpa1']+" | "+data['sgpa2']+" | "+data['sgpa3']+" | "+data['sgpa4']+" | "+data['sgpa5']+" )";
            var rank_text1="( "+data['rank_sem1']+" | "+data['rank_sem2']+" | "+data['rank_sem3']+" | "+data['rank_sem4']+" | "+data['rank_sem5']+" )";
            var dr_text1="( "+data['dr_rank_sem1']+" | "+data['dr_rank_sem2']+" | "+data['dr_rank_sem3']+" | "+data['dr_rank_sem4']+" | "+data['dr_rank_sem5']+" )";
            gpa_text.innerHTML=gpa_text1;
            dr_text.innerHTML=dr_text1;
            rank_text.innerHTML=rank_text1;
          }

          load_chart_1(label_sem,data_rank);
          load_chart_2(label_sem,data_dr_rank);
          load_chart_3(label_sem,data_gpa);
          load_chart_4(label_sem,data_gpa);
        }
        $("#hidethisyo").hide();
      });

  });

    $('#sidebarCollapse').click(function () {
        $('#sidebar').toggleClass('active');
    });
    $('#sidebarCollapse2').click(function () {
        $('#sidebar').toggleClass('active');
    });
});
